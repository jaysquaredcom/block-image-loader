//
//  URLImageLoadOperation.m
//  Andrew Zimmer
//
//  Created by Andrew Zimmer on 9/30/11.
//  Copyright (c) 2011 Andrew Zimmer. All rights reserved.
//

#import "URLImageLoadOperation.h"

@implementation URLImageLoadOperation

// method to return a static cache reference (ie, no need for an init method)


-(void)dealloc {
    [loadCompleteHandler release];
    [loadUrl release];
    [super dealloc];
}

#pragma mark - Public Methods
-(void)loadImageWithURL:(NSString*)url withLoadProgressHandler:(URLImageLoadProgressHandler)progressHandler withLoadCompleteHandler:(URLImageLoadCompleteHandler)completeHandler {
    UIImage *curImage = [self getCacheImage:url];
    
    if(curImage != nil) {
        
        
        if(progressHandler != nil) {
            progressHandler(1, url);
        }
        completeHandler(curImage, url);
    } else {
        
        
        loadCompleteHandler = [completeHandler copy];
        loadProgressHandler = [progressHandler copy];
        if(loadProgressHandler)  {
            loadProgressHandler(0.0f, url);
        }
        
        loadUrl = [[NSString alloc] initWithString:url];
        expectedContentLength = NSURLResponseUnknownLength;
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:5.0];
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        if(connection) {
            receivedData = [[NSMutableData data] retain];
        }
    }
}

-(void)clearCache {
    //todo: make
}

#pragma mark NSURLConnection Delegate Methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [receivedData setLength:0];
    expectedContentLength = [response expectedContentLength];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [receivedData appendData:data];
    if(expectedContentLength != NSURLResponseUnknownLength && loadProgressHandler != nil) {
        loadProgressHandler((float)[receivedData length] / (float)expectedContentLength, loadUrl);
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    UIImage *downloadedImage = [UIImage imageWithData:receivedData];
    
    [self cacheImage:receivedData forURL:loadUrl];
    
    
    if(loadProgressHandler != nil) {
        loadProgressHandler(1, loadUrl);
    }
    
    loadCompleteHandler(downloadedImage, loadUrl);
    [receivedData release];
    [connection release];
    [loadUrl release];
}

-(UIImage*)getCacheImage:(NSString*)url {
    
    NSMutableDictionary *cache = [[NSUserDefaults standardUserDefaults] objectForKey:@"image_cache"];
    
    if (!cache)
        return nil;
    
    if ([cache objectForKey:url]) {
        
        NSString *imagePath = [[self getCachePath] stringByAppendingString:[cache objectForKey:url]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
        
            return [UIImage imageWithContentsOfFile:imagePath];
        } else {
            return nil;
        }
    }
    
    return nil;
    
}

-(void)cacheImage:(NSData*)data forURL:(NSString*)url {

    NSString *filename = [[NSUUID UUID] UUIDString];
    if ([url pathExtension])
        filename = [filename stringByAppendingString:[NSString stringWithFormat:@".%@", [url pathExtension]]];
    
    NSString *imagePath = [[self getCachePath] stringByAppendingString:filename];
    
    [data writeToFile:imagePath atomically:YES];
    
    
    
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:@"image_cache"]];
    
    //DLog(@"%@", dict);
    
    if (!dict)
        dict = [NSMutableDictionary dictionary];
    
    [dict setObject:filename forKey:url];
    
    [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"image_cache"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString*)getCachePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *imageCacheDirPath = [docsPath stringByAppendingPathComponent:@"Image"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath: imageCacheDirPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:imageCacheDirPath
                                  withIntermediateDirectories:NO
                                                   attributes:nil
                                                        error:NULL];
    
    return imageCacheDirPath;
}

@end
